# ADSpreferences

Collection of Matlab scripts to configure preferences in matlab such that it can call Advanced Design System's hpeesof.exe and momwrapper.exe

## Preferences

The following preferences need to be set (default value between brackets)

* `ADS.hpeesofdir` points to the folder where ADS is installed (`C:/Program Files/Keysight/ADS2016_01`) 
* `ADS.simarch` indicates the computer architecture (`win32_64`).
* `ADS.version` indicates the installed ADS version (`2016.01`)
* `ADS.simulationFolder` indicates the folder where temporary simulation data can be stored. (`C:\Simulation`)
* `ADS.parallelSims` tells how many simulations can be run in parallel on a computer (`3`)
* `ADS.output`  when output is set to true, the simulator output text is shown on the matlab command line (`true`).

When the preferences are set, the `checkPrefs` function can get these preferences:

```matlab 
[SIMFOLDER,HPEESOFDIR,SIMARCH,ADSVERSION,PARALLEL,OUTPUT]=checkPrefs()
```

## Set preference scripts

To help with the setting of these preferences, we provide a collection of scripts that set some default preferences. You can edit them to suit your needs.

